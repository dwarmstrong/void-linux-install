#!/usr/bin/env bash
# Purpose: Distro-agnostic functions for Linux setup scripts
# Author : Daniel Wayne Armstrong <hello@dwarmstrong.org>

# ANSI escape codes
RED="\\033[1;31m"
GREEN="\\033[1;32m"
YELLOW="\\033[1;33m"
BLUE="\\033[1;34m"
PURPLE="\\033[1;35m"
NC="\\033[0m" # no colour


tk_red() {
  echo -e "${RED}${1}${NC}"
}


tk_green() {
  echo -e "${GREEN}${1}${NC}"
}


tk_yellow() {
  echo -e "${YELLOW}${1}${NC}"
}


tk_blue() {
  echo -e "${BLUE}${1}${NC}"
}


tk_purple() {
  echo -e "${PURPLE}${1}${NC}"
}


tk_au_revoir() {
  message=$1
  width=100
  if hash cowsay 2>/dev/null && hash lolcat 2>/dev/null; then
    echo ""
    echo "${message}" | cowsay -W "${width}" -f tux | lolcat 2>/dev/null
  elif hash cowsay 2>/dev/null; then
    echo ""
    echo "${message}" | cowsay -W "${width}" -f tux
  else
    echo ""
    echo "${message}"
  fi
}


tk_backup() {
  for f in "$@"; do cp "${f}" "${f}.$(date +'%Y-%m-%dT%H%M%S').bak"; done
}


tk_banner() {
  echo ""
  echo -e "\n${YELLOW}[X] ----------------[  ${1}  ]---------------- ${NC}\n"
  echo ""
}


tk_countdown() {
  # Display countdown from $start to 0
  local start
  start="${1}"
  while [ "${start}" -ge 0 ]; do
    echo -n "${start}...."
    start=$((start - 1))
    sleep 1
  done
}


tk_err() {
  echo -e "\n${RED}error: $1 $NC"
}


tk_verify_net() {
  cmd="ping -w 5 -c 3 google.com"
  if ! $cmd > /dev/null; then
    tk_err "Command \"${cmd}\" failed. Check network connection."
    exit 1
  fi
}


tk_verify_root() {
  if (( EUID != 0 )); then
    tk_err "Permission denied. Are you root?"
    exit 1
  fi
}


tk_verify_uefi() {
  local dirname
  dirname="/sys/firmware/efi/efivars"
  if [[ ! -d "${dirname}" ]]; then
    err "${dirname} not found"
    exit 1
  fi
}
