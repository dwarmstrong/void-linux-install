#!/usr/bin/env bash
# Purpose: Chroot functions for a fresh install of Void Linux
# Author : Daniel Wayne Armstrong <hello@dwarmstrong.org>
set -euo pipefail


config_hostname() {
  tk_banner "Configure hostname as $hostname"
  echo $hostname > /etc/hostname
}


config_hosts() {
  tk_banner "Configure hosts"
cat <<EOF > /etc/hosts
# <ip-address> <hostname.domain.org>   <hostname>
127.0.0.1     localhost.localdomain   localhost
::1           localhost.localdomain   localhost ip6-localhost
127.0.1.1     ${hostname}.localdomain     $hostname
EOF
}


config_keyboard_font() {
  tk_banner "Configure keyboard as $keymap"
  conf="/etc/rc.conf"
  echo "" >> $conf
  echo "# My settings" >> $conf
  echo "KEYMAP=\"${keymap}\"" >> $conf
  tk_banner "Set font to $font"
  echo "FONT=\"${font}\"" >> $conf
}


config_timezone() {
  tk_banner "Configure timezone as $timezone"
  ln -sf /usr/share/zoneinfo/${timezone} /etc/localtime
  hwclock --systohc
}


config_locale() {
  tk_banner "Configure locale as $locale"
  echo LANG=${locale} > /etc/locale.conf
  echo LC_COLLATE=C >> /etc/locale.conf
  echo "${locale} UTF-8" >> /etc/default/libc-locales
  echo "en_US.UTF-8 UTF-8" >> /etc/default/libc-locales
  xbps-reconfigure -f glibc-locales
}


config_fstab() {
  tk_banner "Configure fstab"
  root_id="subvolid=$(btrfs subvolume list / | awk 'FNR == 1 {print $2}')"
  home_id="subvolid=$(btrfs subvolume list / | grep @home | awk '{print $2}')"
  cache_id="subvolid=$(btrfs subvolume list / | grep @cache | awk '{print $2}')"
  log_id="subvolid=$(btrfs subvolume list / | grep @log | awk '{print $2}')"
  snap_id="subvolid=$(btrfs subvolume list / | grep @snapshots | awk '{print $2}')"
cat <<EOF > /etc/fstab
# <file system>       <dir>       <type>  <options>                                   <dump> <pass>
UUID=$cryptomap_uuid   /           btrfs   ${btrfs_opts},${root_id},subvol=/@          0       0
UUID=$cryptomap_uuid   /home       btrfs   ${btrfs_opts},${home_id},subvol=/@home      0       0
UUID=$cryptomap_uuid   /var/cache  btrfs   ${btrfs_opts},${cache_id},subvol=/@cache     0       0
UUID=$cryptomap_uuid   /var/log    btrfs   ${btrfs_opts},${log_id},subvol=/@log       0       0
UUID=$cryptomap_uuid   /.snapshots btrfs   ${btrfs_opts},${snap_id},subvol=/@snapshots 0       0
UUID=$efi_uuid        /efi        vfat    defaults                                    0       0
tmpfs                 /tmp        tmpfs   defaults,nosuid,nodev                       0       0
EOF
}


config_sshd() {
  tk_banner "Configure sshd"
  [ ! -e ${runsvdir}/sshd ] && ln -s /etc/sv/sshd ${runsvdir}/
}


install_ntp() {
  tk_banner "Install ntp client: chrony"
  xbps-install -y chrony
  [ ! -e ${runsvdir}/chronyd ] && ln -s /etc/sv/chronyd ${runsvdir}/
}


install_net() {
  tk_banner "Install network manager: NetworkManager and iwd"
  # Install NetworkManager and iNet Wireless Daemon
  xbps-install -y NetworkManager iwd
  mkdir -p /etc/NetworkManager/conf.d/
cat <<EOF >> /etc/NetworkManager/conf.d/wifi_backend.conf
[device]
wifi.backend=iwd
wifi.iwd.autoconnect=yes
EOF
  [ ! -e ${runsvdir}/dbus ] && ln -s /etc/sv/dbus ${runsvdir}/
  [ ! -e ${runsvdir}/iwd ] && ln -s /etc/sv/iwd ${runsvdir}/
  [ ! -e ${runsvdir}/NetworkManager ] && ln -s /etc/sv/NetworkManager ${runsvdir}/
}


install_zram() {
  tk_banner "Set zram for swap memory"
  conf="/etc/sv/zramen/conf"
  xbps-install -y zramen
  tk_backup $conf
cat <<EOF > $conf
export ZRAM_COMP_ALGORITHM='lz4'
export ZRAM_PRIORITY=32767
export ZRAM_SIZE=25
export ZRAM_MAX_SIZE=4096
export ZRAM_STREAMS=1
EOF
  [ ! -e ${runsvdir}/zramen ] && ln -s /etc/sv/zramen ${runsvdir}/
}


install_session_mgt() {
  tk_banner "Install session manager: elogind and d-bus"
  xbps-install -y elogind dbus-elogind dbus-elogind-libs dbus-elogind-x11 polkit
  [ ! -e ${runsvdir}/dbus ] && ln -s /etc/sv/dbus ${runsvdir}/
  [ ! -e ${runsvdir}/polkitd ] && ln -s /etc/sv/polkitd ${runsvdir}/
}


install_cron() {
  tk_banner "Install cron daemon: snooze"
  xbps-install -y snooze
  [ ! -e ${runsvdir}/snooze-daily ] && ln -s /etc/sv/snooze-daily ${runsvdir}/
}


install_repo_nonfree() {
  tk_banner "Install nonfree subrepo for proprietary firmware"
  xbps-install -y void-repo-nonfree
  xbps-install -S
}


install_firmware() {
  tk_banner "Install firmware"
  if grep AuthenticAMD /proc/cpuinfo; then
    xbps-install -y linux-firmware-amd
  else
    xbps-install -y intel-ucode
  fi
}


install_xorg() {
  tk_banner "Install xorg"
  xbps-install -y xorg xbindkeys xvkbd xinput xterm
  if ! grep AuthenticAMD /proc/cpuinfo; then
    # Install the OpenGL driver:
    xbps-install -y mesa-dri
    # Install the Khronos Vulkan Loader:
    xbps-install -y vulkan-loader mesa-vulkan-intel
    # Video acceleration:
    xbps-install -y intel-video-accel
    # For newer Intel chipsets, the DDX drivers may interfere with correct operation. This is
    # characterized by graphical acceleration not working and general graphical instability.
    # If this is the case, see:
    # https://docs.voidlinux.org/config/graphical-session/graphics-drivers/intel.html
  fi
}


install_openbox() {
  tk_banner "Install openbox"
  pkgs="openbox obconf dunst feh hsetroot i3lock libnotify lxappearance lximage-qt picom rofi "
  pkgs+="scrot tint2 volumeicon xfce4-power-manager xfce4-terminal"
  xbps-install -y $pkgs
}


install_desktop_pkgs() {
  tk_banner "Install desktop packages"
  av="pipewire wireplumber alsa-utils pavucontrol vlc"
  devel="git gnupg shellcheck"
  pyenv="base-devel libffi-devel bzip2-devel openssl openssl-devel readline readline-devel "
  pyenv+="sqlite-devel xz liblzma-devel zlib zlib-devel"
  doc="qpdfview"
  font="dejavu-fonts-ttf font-fira-ttf liberation-fonts-ttf ttf-ubuntu-font-family"
  image="gimp gthumb"
  libvirt="libvirt virt-manager qemu polkit"
  net="firefox network-manager-applet newsboat thunderbird transmission-gtk"
  theme="adwaita-icon-theme papirus-icon-theme qt5-styleplugins"
  
  no_bitmaps="/etc/fonts/conf.d/70-no-bitmaps.conf"
  pipewired="/etc/pipewire/pipewire.conf.d"
  conf="10-wireplumber.conf"
  
  xbps-install -y $av $devel $pyenv $doc $font $image $libvirt $net $theme
  # Disable use of bitmap fonts:
  [ ! -e $no_bitmaps ] && ln -s /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf $no_bitmaps
  xbps-reconfigure -f fontconfig
  # Session manager
  # In pipewire, a session manager assumes responsibility for interconnecting media sources
  # and sinks as well as enforcing routing policy. Without a session manager, pipewire will
  # not function. Session manager used is _wireplumber_.
  #
  # Be aware that wireplumber must launch --after-- the pipewire executable.
  #
  # Configure pipewire to launch the session manager directly:
  [ ! -d $pipewired ] && mkdir -p $pipewired
  [ ! -e ${pipewired}/${conf} ] && ln -s /usr/share/examples/wireplumber/${conf} ${pipewired}/
  # See: https://docs.voidlinux.org/config/media/pipewire.html
}


openbox_profile() {
  install_xorg
  install_openbox
  install_desktop_pkgs
}


install_log() {
  tk_banner "Install syslog daemon: socklog-void"
  # An account for $username must exist before running this function.
  xbps-install -y socklog-void
  [ ! -e ${runsvdir}/socklog-unix ] && ln -s /etc/sv/socklog-unix ${runsvdir}/
  [ ! -e ${runsvdir}/nanoklogd ] && ln -s /etc/sv/nanoklogd ${runsvdir}/
}


config_grub() {
  tk_banner "Configure GRUB"
  conf="/etc/default/grub"
  tk_backup $conf
cat <<EOF > $conf
GRUB_DEFAULT=0
GRUB_TIMEOUT=5
GRUB_TIMEOUT_STYLE="menu"
GRUB_DISABLE_SUBMENU=true
GRUB_DISTRIBUTOR="Void"
# Extra parameters to be passed on the kernel command line for all Linux menu entries:
GRUB_CMDLINE_LINUX="rd.luks.uuid=${luks_uuid}"
# Extra parameters to be passed on the kernel command line for non-recovery Linux menu entries:
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=4 rd.auto=1"
# Check for encrypted disks and generate additional commands needed to access them during boot:
GRUB_ENABLE_CRYPTODISK=y
# Set desired menu colors. Entries specified as foreground/background:
GRUB_COLOR_NORMAL="white/black"
GRUB_COLOR_HIGHLIGHT="white/green"
#GRUB_BACKGROUND="/boot/grub/background.png"
#GRUB_GFXMODE=1024x768
EOF
}


install_grub() {
  tk_banner "Install bootloader: GRUB"
  grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=Void
  grub-mkconfig -o /boot/grub/grub.cfg
}


config_root() {
  tk_banner "Set root password"
  passwd
  chsh -s /bin/bash root
}


create_user() {
  tk_banner "Create user account for $username"
  groups="wheel,libvirt,kvm,socklog"
  useradd -m -G wheel -s /bin/bash $username
  passwd $username
  echo "$username ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/sudoer_${username}
}


config_keyfile() {
  tk_banner "Generate crypto keyfile (and skip entering passphrase twice on boot)"
  keyfile="/boot/keyfile.bin"
  dd bs=512 count=4 iflag=fullblock if=/dev/random of=${keyfile}
  chmod 000 $keyfile

  echo "Unlock encrypted $luks_part and fill 2nd key slot with $keyfile as key:"
  cryptsetup -v luksAddKey $luks_part $keyfile
  echo "cryptdev UUID=${luks_uuid} $keyfile luks" >> /etc/crypttab
  echo 'install_items+=" /boot/keyfile.bin /etc/crypttab "' > /etc/dracut.conf.d/10-crypt.conf
}


finish_up() {
  tk_banner "Configure packages and generate initramfs"
  # Ensure all installed packages are configured properly and a working initramfs is generated:
  xbps-reconfigure -fa
}


au_revoir() {
  message="Done! Void is in tip-top form and ready to reboot."
  echo ""
  echo "${message}" | /usr/bin/cowsay -W 100 -f tux | /usr/bin/lolcat 2>/dev/null
}


main() {
  script=$(basename $0)
  efi_uuid=$(blkid -s UUID -o value $efi_part)
  luks_uuid=$(blkid -s UUID -o value $luks_part)
  cryptomap_uuid=$(blkid -s UUID -o value $cryptomap)
  runsvdir="/etc/runit/runsvdir/default"

  # ... train kept a rollin' ...
  /usr/bin/sl || true
  config_hostname
  config_hosts
  config_keyboard_font
  config_timezone
  config_locale
  config_fstab
  config_sshd
  install_ntp
  install_net
  install_zram
  install_session_mgt
  install_cron
  install_repo_nonfree
  install_firmware
  if [[ "$profile" == "openbox" ]]; then
    openbox_profile
  fi
  install_log
  config_grub
  install_grub
  config_root
  create_user
  config_keyfile
  finish_up
  au_revoir
}

# (O< -- Let's go!
# (/)_
source toolkit.sh   # distro-agnostic functions - prefixed with `tk_*` - for setup scripts
main
