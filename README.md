## Introduction

**NOTE:** (2024-01-05) I have since moved on to using [Debian 12 with encrypted Root-on-ZFS](https://www.dwarmstrong.org/debian-install-zfs/).

**void-linux-install** installs [Void Linux](https://voidlinux.org/) with root filesystem encryption.

## Prerequisites

* Target device is x86_64-glibc architecture with UEFI boot

## Summary

* Install Void Linux as the sole operating system on a single disk
* GPT partition table with two partitions:
  * EFI system partition
  * encrypted root partition
* BTRFS root filesystem with multiple subvolumes
* Choice of system profile: _minimal_ (console-only) or _desktop_ ([Openbox](https://www.dwarmstrong.org/openbox/))
* GRUB bootloader

## How does it work?

1. Download the current [void-live-x86_64-202xxxxx-base.iso](https://repo-default.voidlinux.org/live/current/) and [prepare installation media](https://docs.voidlinux.org/installation/live-images/prep.html). **Tip:** I use [Ventoy](https://www.dwarmstrong.org/ventoy/).

2. Boot installer. Login `root:voidlinux`.

3. Default shell is `/bin/sh`. Switch to `bash`:

```
# bash
bash-5.2#
```

4. Update package manager and install `git`:

```
bash-5.2# xbps-install -S
bash-5.2# xbps-install -uy xbps
bash-5.2# xbps-install git
```

5. Download:

```
bash-5.2# git clone https://gitlab.com/dwarmstrong/void-linux-install.git
```

6. Change directory and run `install.sh` script:

```
bash-5.2# cd void-linux-install
bash-5.2# ./install.sh
```

## Author

[Daniel Wayne Armstrong](https://www.dwarmstrong.org)

## License

GPLv3. See [LICENSE](https://gitlab.com/dwarmstrong/void-linux-install/-/blob/main/LICENSE.md) for more details.
