#!/usr/bin/env bash
# Purpose: Install Void Linux with root filesystem encryption
# Author : Daniel Wayne Armstrong <hello@dwarmstrong.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the LICENSE file for more details.
set -euo pipefail


greeting() {
  clear
  echo ""
  tk_green "(O< -- Hello! \"$(basename $0)\" installs Void Linux with root filesystem encryption."
  tk_green "(/)_"
  echo ""
  echo "See README for details."
  echo ""
  tk_red "#### ALL CURRENT DATA ON THE DISK WILL BE LOST ####"
  echo ""
  while :
  do
    read -r -n 1 -p "Proceed? [Yn] "
    if [[ "$REPLY" == [yY] || "$REPLY" == "" ]]; then
      echo ""
      echo "OK! Let's roll ..."
      break
    elif [[ "$REPLY" == [nN] ]]; then
      echo ""
      exit
    else
      tk_err "Please select 'y' or press ENTER to proceed, or 'n' to quit."
    fi
  done
}


checklist() {
  tk_verify_net
  tk_verify_root
  tk_verify_uefi
}


update_package_manager() {
  tk_banner "Update package manager"
  xbps-install -S
  xbps-install -uy xbps
}


partition_disk() {
  tk_banner "Partition $disk"
  # Install disk tools
  xbps-install -Sy gptfdisk parted
  
  # Delete old partition layout
  wipefs -af $disk
  sgdisk --zap-all --clear $disk
  partprobe $disk
  
  # Partition disk
  # In lieu of using a swapfile or dedicated swap partition as system swap,
  # later in the install a swap device is created in RAM using zram.
  sgdisk -n 0:0:+512MiB -t 0:ef00 -c 0:esp $disk
  sgdisk -n 0:0:0 -t 0:8309 -c 0:luks $disk
  partprobe $disk
}


encrypt_partition() {
  tk_banner "Encrypt $luks_part"
  # In all circumstances, LUKS1 is successful with GRUB, so that is what I use. 
  cryptsetup --type luks1 -v -y luksFormat $luks_part
}


format_partitions() {
  tk_banner "Format $efi_part and $cryptomap"
  mkfs.vfat -F32 -n ESP $efi_part
  cryptsetup open $luks_part $crypto
  mkfs.btrfs -L rootfs $cryptomap
  # Labels are optional, but helpful. They allow for easy mounting without a UUID.
}


create_btrfs_subvolumes() {
  tk_banner "Create BTRFS subvolumes"
  # Mount root filesystem
  mount $cryptomap /mnt
  
  # Create subvolumes
  # Changing subvolume layouts is made simpler by not mounting the top-level subvolume
  # as / (default). Instead, create the @ subvolume that contains the actual data and
  # mount that to /.
  btrfs subvolume create /mnt/@
  btrfs subvolume create /mnt/@home
  btrfs subvolume create /mnt/@cache
  btrfs subvolume create /mnt/@log
  btrfs subvolume create /mnt/@snapshots
}


mount_btrfs_subvolumes() {
  tk_banner "Mount BTRFS subvolumes"
  umount /mnt
  mount -o ${btrfs_opts},subvol=@ $cryptomap /mnt
  mkdir -p /mnt/{home,var/cache,var/log,.snapshots}
  mount -o ${btrfs_opts},subvol=@home $cryptomap /mnt/home
  mount -o ${btrfs_opts},subvol=@cache $cryptomap /mnt/var/cache
  mount -o ${btrfs_opts},subvol=@log $cryptomap /mnt/var/log
  mount -o ${btrfs_opts},subvol=@snapshots $cryptomap /mnt/.snapshots
}


mount_efi_partition() {
  tk_banner "Mount EFI partition"
  mkdir /mnt/efi
  mount LABEL=ESP /mnt/efi
}


copy_rsa_keys() {
  tk_banner "Copy RSA keys"
  mkdir -p /mnt/var/db/xbps/keys
  cp /var/db/xbps/keys/* /mnt/var/db/xbps/keys/
}


install_base_system() {
  tk_banner "Install base system"
  repo="https://repo-default.voidlinux.org/current"
  base_pkgs="base-system btrfs-progs cryptsetup grub-x86_64-efi bash-completion cowsay htop "
  base_pkgs+="lm_sensors ncdu lolcat-c nano neofetch neovim openssh plocate rsync sl snooze "
  base_pkgs+="terminus-font tmux tree udisks2 unzip whois xtools"
  XBPS_ARCH=x86_64 xbps-install -Sy -R $repo -r /mnt $base_pkgs
}


enter_chroot() {
  cp ${PWD}/files/chroot.sh /mnt/
  cp ${PWD}/files/toolkit.sh /mnt/
  
  # Pass variables to chroot
  export disk=$disk
  export profile=$profile
  export hostname=$hostname
  export username=$username
  export keymap=$keymap
  export font=$font
  export timezone=$timezone
  export locale=$locale
  export crypto=$crypto
  export cryptomap=$cryptomap
  export btrfs_opts=$btrfs_opts
  export efi_part=$efi_part
  export luks_part=$luks_part
  
  xchroot /mnt ./chroot.sh
  rm /mnt/chroot.sh
  rm /mnt/toolkit.sh
}


finish_up() {
  doi=$(date +'%Y-%m-%dT%H%M%S')
  echo "${doi}UTC" > /mnt/etc/date_of_install
  echo ""
  echo "To finish up ..."
  echo ""
  echo "* Unmount all mounted filesystems: \"umount -R /mnt\""
  echo "* Close cryptdevice: \"cryptsetup close $crypto\""
  echo "* Reboot: \"reboot\""
  exit
}


main() {
  script=$(basename $0)
  query=8
  disk="foo"
  profile="minimal"
  hostname="foobox"
  username="foo"
  keymap="en"
  font="ter-v22n"
  timezone="America/Toronto"
  locale="en_CA.UTF-8"
  crypto="cryptdev"
  cryptomap="/dev/mapper/${crypto}"
  btrfs_opts="rw,noatime,compress-force=zstd:1,space_cache=v2"
  
  greeting
  checklist
  while :
  do
    tk_banner "Question 1 of $query"
    echo "Disks:"
    echo ""
    lsblk --noheadings --nodeps --output NAME | grep --invert-match -e loop -e sr -e zram
    echo ""
    echo "Enter a disk name from the above list where Void will be installed"
    read -r -p "> "
    disk="/dev/${REPLY}"

    tk_banner "Question 2 of $query"
    while :
    do
      echo "Setup this computer as:"
      echo "[1] Minimal (console-only) [2] Desktop (Openbox)"
      echo "Choose a number"
      read -r -n 1 -p "> "
      if [[ "$REPLY" == 1 ]]; then
        profile="minimal"
        break
      elif [[ "$REPLY" == 2 ]]; then
        profile="openbox"
        break
      else
        tk_err "Please select 1 or 2."
      fi
    done
    echo ""

    tk_banner "Question 3 of $query"
    while :
    do
      echo "Enter a hostname for the new system"
      read -r -p "> "
      if [[ -z "$REPLY" ]]; then
        tk_err "Please enter a name."
      else
        hostname=$REPLY
        break
      fi
    done

    tk_banner "Question 4 of $query"
    while :
    do
      echo "Enter a username for the non-root user account"
      read -r -p "> "
      if [[ -z "$REPLY" ]]; then
        tk_err "Please enter a name."
      else
        username=$REPLY
        break
      fi
    done

    tk_banner "Question 5 of $query"
    echo "Enter a keymap (press ENTER to accept default keymap: \"$keymap\")"
    read -r -p "> "
    if [[ -n "$REPLY" ]]; then
      keymap=$REPLY
    fi

    tk_banner "Question 6 of $query"
    echo "Enter the timezone (press ENTER to accept default timezone: \"$timezone\")"
    read -r -p "> "
    if [[ -n "$REPLY" ]]; then
      timezone=$REPLY
    fi

    tk_banner "Question 7 of $query"
    echo "Enter the locale (press ENTER to accept default locale: \"$locale\")"
    read -r -p "> "
    if [[ -n "$REPLY" ]]; then
      locale=$REPLY
    fi

    tk_banner "Question $query of $query"
    echo "Disk    : $disk"
    echo "Profile : $profile"
    echo "Hostname: $hostname"
    echo "Username: $username"
    echo "Keymap  : $keymap"
    echo "Timezone: $timezone"
    echo "Locale  : $locale"
    echo ""
    while :
    do
      read -r -n 1 -p "Is this correct? [Ynq] > "
      if [[ "$REPLY" == [yY] || "$REPLY" == "" ]]; then
        echo ""
        echo ""
        echo "OK! Starting in"
        tk_countdown 3
        break 2
      elif [[ "$REPLY" == [nN] ]]; then
        echo ""
        tk_yellow "Let's try again ..."
        break
      elif [[ "$REPLY" == [qQ] ]]; then
        echo ""
        exit
      else
        tk_err "Please select 'y' to proceed, 'n' to redo questions, or 'q' to quit."
        continue
      fi
    done
  done

  if [[ $disk == *"nvme"* ]]; then
    efi_part="${disk}p1"
    luks_part="${disk}p2"
  else
    efi_part="${disk}1"
    luks_part="${disk}2"
  fi

  update_package_manager
  partition_disk
  encrypt_partition
  format_partitions
  create_btrfs_subvolumes
  mount_btrfs_subvolumes
  mount_efi_partition
  copy_rsa_keys
  install_base_system
  enter_chroot
  finish_up
}


# (O< -- Let's go!
# (/)_
source files/toolkit.sh   # distro-agnostic functions - prefixed with `tk_*` - for setup scripts
main
